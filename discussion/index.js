//console.log('hello From JS');

//Dom Selector

//first you have to describe the location whre in its going to target/acces elements.

//identify the 'Atttibute' and 'Value' to properly recognixe and identify which element to target
//visualize the statement in JS
//let/const:jsObject = {}

//[SECTION] querySelector
// const firstName = document.querySelector('#firstName');
// const lastName = document.querySelector('#lastName');

 
//[SECTION] getElement Functions
//getElementById => target a single component
// const firstName = document.getElementById('firstName');
// const lastName = document.getElementById('lastName');

//getElementsByClassName => can be essential when targeting multiple components at the same time.
const inputFields = document.getElementsByClassName('form-control');

//getElementsTagName => can be used when targeting elements of the same tags.
const heading = document.getElementsByTagName('h3')
console.log(heading);

//check if you were able to succesfully target an element from document
console.log(firstName);
console.log(lastName);
console.log(inputFields);

//check the type of data that we target from document
//console.log(typeof firstName);


//get the information from the input fields
//target the value property of the object.
//console.log(firstName.value);

//object